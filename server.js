var express = require('express');
var app  = express();
var path = require('path')
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var db = require('monk')('karim1995:karim123@ds056979.mlab.com:56979/test_app');
var morgan = require('morgan');
var jwt    = require('jsonwebtoken'); 
var config = require('./config');
authToken = null

var port = process.env.PORT || 3000;
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(methodOverride('X-HTTP-Method-Override')); 
//app.use(express.static(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')))
app.set('superSecret', config.secret)


app.post('/authenticate',function(req,res){

	var collection = db.get('login')
	collection.findOne({
	    username: req.body.name
	  }, function(err, user) {

	    if (err) throw err;

	    if (!user) {
	      res.json({ success: false, message: 'Authentication failed. User not found.' });
	    } else if (user) {

	      // check if password matches
	      if (user.password != req.body.password) {
	        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
	      } else {
	       
	        var token = jwt.sign(user, app.get('superSecret'), {
	          expiresIn : 60*60*24 // expires in 24 hours
	        });
	        authToken = token
	        res.json({success: true,message: 'Enjoy your token!',token: token,user:user});
	      }   

	    }

	  });

})


app.use(function(req, res, next) {
  var token = authToken

  // decode token
  if (token) {
    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } else {
        req.decoded = decoded;    
        next();
      }
    });

  } else {

    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });
    
  }
});



app.post('/addflatdetails',function(req,res){
	console.log(req.body,"from server")
	var collection = db.get('flatdetails')
	collection.findOne({flatname:req.body.flatname,flatnumber:req.body.flatnumber},function(err,doc){
		if(doc){
			res.json({success:false,message:"Record already exists "})
		}else{

		collection.insert(req.body,function(err,doc){
		if(err){
			console.log(err)
		}else{
			res.json({success:true,message:"Record Added successfully"})
		}
		})
			
	}
})
})


app.post('/verifyflatdetails',function(req,res){
	console.log(req.body)
	var collection = db.get('flatdetails')
	collection.findOne({flatname:req.body.flatname, flatnumber:req.body.flatnumber},function(err,doc){
		if(err){
			console.log(err)
		}
		if(doc){
			res.json({success:true,message:"Flat Details Verified"})
		}
		if(!doc){
			console.log(doc)
			res.json({success:false,message:"Flat Details are incorrect"})
		}
	})
})

app.get('/getflatholders',function(req,res){
	var collection = db.get('flatdetails')
	collection.find({},function(err,doc){
		if(err){
			console.log(err)
		}else{
			console.log(doc)
			res.json({success:true,message:"successfully fetch user details",users:doc})
		}
	})
})


app.post('/addcredit',function(req,res){
	let detail = req.body
	let balance =0
	console.log(detail)
	var collection = db.get('flatdetails')
	collection.findOne({
		flatname:req.body.flatdetails.flatname,
		flatnumber:req.body.flatdetails.flatnumber
	},function(err,doc){
		if(err){
			console.log(err)
		}else{
			balance = doc.amountdue - req.body.credit
			if(balance < 0){
				detail.balance = 0
				collection.update({
						flatname:req.body.flatdetails.flatname,
						flatnumber:req.body.flatdetails.flatnumber
					},{
						$push : {
							"maintencehistory":{
								entry : detail
							}
						}
					}, function(err,doc){
						if(err){
							console.log(err)
						}else{
							res.json({success:true, message:"Maintence has been added"})
							console.log("maintencehistory updated",doc)
						}
					})
				

			}else{

			collection.update({
				flatname:req.body.flatdetails.flatname,
				flatnumber:req.body.flatdetails.flatnumber
			},{
				$set : {
					amountdue : balance
				}
			},function(err,doc){
				if(err){
					console.log(err)
				}else{
					console.log("update doc is ",doc)
					detail.balance = balance
					collection.update({
						flatname:req.body.flatdetails.flatname,
						flatnumber:req.body.flatdetails.flatnumber
					},{
						$push : {
							"maintencehistory":{
								entry : detail
							}
						}
					}, function(err,doc){
						if(err){
							console.log(err)
						}else{

							res.json({success:true, message:"Maintence has been added"})
							console.log("maintencehistory updated",doc)
						}
					})
				}
			})

			}
			//console.log(doc)
		}
	})
})

app.get('/logout',function(req,res){
	authToken =null
	res.json({success:true,message:"Logout successfully"})
})


app.get('/users',function(req,res){
	var collection = db.get('login')
	collection.find({},function(err,doc){
		res.json({users:doc})
	})

})



app.listen(port);	
console.log('Magic happens on port ' + port); 			
exports = module.exports = app; 						