angular.module('starter.controllers', ['toaster','ngAnimate'])

//==================== Login ============= 
.controller('Login', function($scope,myService,$location,toaster) {
	$scope.loadingStatus="none"
	console.log($scope.loadingStatus)



	$scope.login = function(username,password){
		$scope.loadingStatus="block"
		console.log($scope.loadingStatus)
		var data = {
			name:username,
			password:password
		}
		
		//calling token service
		myService.authenticate(data).then(function(res){
			$scope.loadingStatus="none"
			console.log("from controller",res)
			if(res.data.success === false){
				toaster.pop({
					type: 'error',
					title: 'Login',
					body: res.data.message,
					timeout: 3000
				});
			}else{
				toaster.pop({
					type: 'success',
					title: 'Login',
					body: res.data.message,
					timeout: 3000
				});
				console.log(res)
				localStorage.setItem('username',res.data.user.username)
				$location.path('menu')
			}
		},function(err){
			console.log(err)
			toaster.pop({
				type: 'error',
				title: 'Login',
				body: err,
				timeout: 3000
			});
		})

	}
})


//====================== Menu ====================
.controller('Menu', function($scope,myService,$location) {

	$scope.username = localStorage.getItem('username')


})


//======================== Add flat Details ===============
.controller('Addflatholder', function($scope,myService,$location,toaster,$timeout) {

	$scope.loadingStatus = "none"
	$scope.addFlatholderDetails = function(flatName,flatNumber,contactNumber,nicNumuber,flatHolderName,amountdue){
		
		var data = {
			flatname :flatName,
			flatnumber:flatNumber,
			contact : contactNumber,
			nic : nicNumuber,
			flatholdername:flatHolderName,
			amountdue:amountdue,
			date : new Date()
		}
		$scope.loadingStatus = "block"
		myService.addflatdetails(data).then(function(res){
		$scope.loadingStatus = "none"
		console.log(res)
		if(res.data.success === false){
			toaster.pop({
					type: 'error',
					title: 'Add Flat Holder',
					body: res.data.message,
					timeout: 3000
				});
		}
		else{
			toaster.pop({
					type: 'success',
					title: 'Add Flat Holder',
					body: res.data.message,
					timeout: 3000
				})
			//$timeout(function(){$location.path('menu')},3001)

		}
		},function(err){
			console.log(err)
		})
	}
	

})


//=============================View Flat holder details=======
.controller('Viewflatholder', function($scope,myService,$location) {

  $scope.users = []

  myService.getflatholders().then(function(res){
  	for(var i=0; i<res.data.users.length; i++){
  		var data = {
  			flatHolderName : res.data.users[i].flatholdername,
  			contact 	   : res.data.users[i].contact,
  			date 		   : res.data.users[i].date
  		}
  		$scope.users.push(data)
  	}
  	console.log($scope.users)



  },function(err){
  	console.log(err)
  })

})



//==========================Add maintence============
.controller('Addmaintence', function($scope,myService,$location,toaster) {
	$scope.loadingStatus="none"
	$scope.formStatus ="none"
	$scope.proccedStatus = "block"
	$scope.maintenceStatus = "none"
	$scope.infoStatus = "block"
	$scope.Month = ['Jan', 'Feb', 'Mar', 'Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'];
	$scope.Year = ['2017','2018','2019']
	var flatdetail = {}
	$scope.verifyflatdetails = function(flatname,flatnumber){
		$scope.loadingStatus="block"
		flatdetail ={
			flatname :flatname,
			flatnumber:flatnumber
		}
		myService.verifyflatdetails(flatdetail).then(function(res){
			$scope.loadingStatus="none"
			console.log(res)
			if(res.data.success == true){
				$scope.formStatus ="block"
				$scope.proccedStatus = "none"
				$scope.maintenceStatus = "block"
				$scope.infoStatus="none"

			}else{
				toaster.pop({
					type: 'error',
					title: 'Add Flat maintence',
					body: res.data.message,
					timeout: 3000
				});

			}
		},function(err){
			console.log(err)
		})
	}


	$scope.addcredit = function(month,year,amount,paidby,complain,flatname,flatnumber){
		$scope.loadingStatus="block"
		var data = {
			month : month,
			year : year,
			debit : 0,
			credit : amount,
			paidby : paidby,
			complain :complain,
			date : new Date()
		}
		data.flatdetails = flatdetail
		console.log(data)
		if(month == undefined || year == undefined || amount == undefined || paidby == null){
			$scope.loadingStatus = "none"
			toaster.pop({
					type: 'error',
					title: 'Add Flat maintence',
					body: "Please Fill the required fields",
					timeout: 8000
				});

		}
		else{
		myService.addcredit(data).then(function(res){
			$scope.loadingStatus="none"
			console.log(res)
			if(res.data.success == true){
				toaster.pop({
					type: 'success',
					title: 'Add Flat maintence',
					body: res.data.message,
					timeout: 3000
				});

			}
		},function(err){

		})
	}

	}
	console.log("maintence loaded")
})
