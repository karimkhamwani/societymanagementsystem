angular.module('starter.services', []).
factory('myService', function($http) {

	 var ergastAPI = {};
  		ergastAPI.authenticate = function (data) { 
            var req = {
                method: 'POST',
                url: '/authenticate',
                data:data                
             }
            return $http(req);
        }
        ergastAPI.addflatdetails = function (data) { 
            var req = {
                method: 'POST',
                url: '/addflatdetails',
                data:data                
             }
            return $http(req);
        }
      ergastAPI.getflatholders = function () { 
            var req = {
                method: 'GET',
                url: '/getflatholders'          
             }
            return $http(req);
        }
      ergastAPI.verifyflatdetails = function (data) { 
            var req = {
                method: 'POST',
                url: '/verifyflatdetails',
                data:data          
             }
            return $http(req);
        }
     ergastAPI.addcredit = function (data) { 
            var req = {
                method: 'POST',
                url: '/addcredit',
                data:data          
             }
            return $http(req);
        }
        return ergastAPI;

});

