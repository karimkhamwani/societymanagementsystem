angular.module('starter', ['ngRoute','starter.controllers','starter.services'])

.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	 $locationProvider.hashPrefix('');
	$routeProvider

		// home page
		.when('/', {
			templateUrl: '/views/login.html',
			controller: 'Login'
		})
		.when('/menu', {
			templateUrl: '/views/menu.html',
			controller: 'Menu'
		})
		.when('/addflatholder', {
			templateUrl: '/views/addflatholder.html',
			controller: 'Addflatholder'
		})
		.when('/viewflatholder', {
			templateUrl: '/views/viewflatholder.html',
			controller: 'Viewflatholder'
		})
		.when('/addmaintence', {
			templateUrl: '/views/addmaintence.html',
			controller: 'Addmaintence'
		})
		.otherwise({
        template : "<h1>None</h1><p>Nothing has been selected</p>"
    	});


}]);